#include "rouedentee.h"
#include "qimage.h"
#include "qopengltexture.h"

#include <QDebug>
#include <QMatrix4x4>
#include <QVector3D>
#include <QColor>

rouedentee::rouedentee(float r1, float r2, float ep_roue, int nb_dents)
{
    m_vbo.create();
    m_vbo.bind();
    this->nb_dents = nb_dents;
    this->r1 = r1;
    this->r2 = r2;
    QVector<GLfloat> vertData;

    QImage lapin(QString(":/lapin.png"));
    QImage loutre(QString(":/loutre.jpg"));
    m_texture[0] = new QOpenGLTexture(lapin);
    m_texture[1] = new QOpenGLTexture(loutre);

    for (int i = 0; i < nb_dents; ++i) {
        buildVertData(vertData, i, ep_roue, nb_dents);
    }
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));
    m_vbo.release();
}

rouedentee::~rouedentee()
{
    m_vbo.destroy();
}

void rouedentee::buildVertData(QVector<GLfloat> &data, int i, float ep_roue, int nb_dents)
{
    float angle = 360.0 / (float) nb_dents;
    float rad = angle * M_PI/180.0;

    QVector<QVector3D> vs;
    QColor colors = QColor::fromRgbF(1,1,1);

    QVector<QVector3D> normals;
    QVector<int> ind_nor;
    //    QVector<int> ind_ver;

    QVector3D A = *new QVector3D(0, 0, -ep_roue/2); // A 0
    QVector3D B = *new QVector3D(r1 * cos(0 + i*rad), r1 * sin(i * rad), -ep_roue/2);     // B 1
    QVector3D C = *new QVector3D(r1 * cos(rad/4 + i * rad), r1 * sin(rad/4 + i * rad), -ep_roue/2); // C 2
    QVector3D D = *new QVector3D(r2 * cos((2*rad)/4 + i * rad), r2 * sin((2*rad)/4 + i * rad), -ep_roue/2); // D 3
    QVector3D E = *new QVector3D(r2 * cos((3*rad)/4 + i * rad), r2 * sin((3*rad)/4 + i * rad), -ep_roue/2); // E 4
    QVector3D F = *new QVector3D(r1 * cos(rad + i * rad), r1 * sin(rad + i * rad), -ep_roue/2); // F 5

    QVector3D A_ = *new QVector3D(0, 0, ep_roue/2); // A' 6
    QVector3D B_ = *new QVector3D(r1 * cos(0 + i*rad), r1 * sin(i * rad), ep_roue/2);     // B' 7
    QVector3D C_ = *new QVector3D(r1 * cos(rad/4 + i * rad), r1 * sin(rad/4 + i * rad), ep_roue/2); // C' 8
    QVector3D D_ = *new QVector3D(r2 * cos((2*rad)/4 + i * rad), r2 * sin((2*rad)/4 + i * rad), ep_roue/2); // D' 9
    QVector3D E_ = *new QVector3D(r2 * cos((3*rad)/4 + i * rad), r2 * sin((3*rad)/4 + i * rad), ep_roue/2); // E' 10
    QVector3D F_ = *new QVector3D(r1 * cos(rad + i * rad), r1 * sin(rad + i * rad), ep_roue/2); // F' 11

    vs = { A, B, C, D, E, F,
           A_, B_, C_, D_, E_, F_,
           B, C, C_, B_,
           C, D, D_, C_,
           D, E, E_, D_,
           E, F, F_, E_
         };

    QVector3D
            vCE     = E - C,
            vBC     = C - B,
            vBC_    = C_ - B,
            vCD     = D - C,
            vCD_    = D_ - C,
            vDE     = E - D,
            vDE_    = E_ - D,
            vEF     = F - E,
            vEF_    = F_ - E;

    QVector3D n     = QVector3D::normal(vCD, vCE);
    QVector3D nBCC_ = QVector3D::normal(vBC_, vBC);
    QVector3D nCDD_ = QVector3D::normal(vCD_, vCD);
    QVector3D nDEE_ = QVector3D::normal(vDE_, vDE);
    QVector3D nEFF_ = QVector3D::normal(vEF_, vEF);

    normals.append({ n, -nBCC_, -nCDD_, -nDEE_, -nEFF_ });

    ind_nor = { 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4};

    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        data.append(vs[i].x());
        data.append(vs[i].y());
        data.append(vs[i].z());
        // couleurs sommets
        data.append(colors.redF());
        data.append(colors.greenF());
        data.append(colors.blueF());
        // normales sommets
        data.append(-normals[0].x());
        data.append(-normals[0].y());
        data.append(-normals[0].z());
        //textures
        float x = ((vs[i].x()/r2)+1)/2;
        float y = ((vs[i].y()/r2)+1)/2;
        data.append(-x);
        data.append(-y);
    }

    for (int i = 6; i < 12; ++i) {
        // coordonnées sommets
        data.append(vs[i].x());
        data.append(vs[i].y());
        data.append(vs[i].z());
        // couleurs sommets
        data.append(colors.redF());
        data.append(colors.greenF());
        data.append(colors.blueF());
        // normales sommets
        data.append(normals[0].x());
        data.append(normals[0].y());
        data.append(normals[0].z());
        // texture
        float x = ((vs[i].x()/r2)+1)/2;
        float y = ((vs[i].y()/r2)+1)/2;
        data.append(-x);
        data.append(-y);
    }

    for (int i = 12; i < 28; ++i) {
        // coordonnées sommets
        data.append(vs[i].x());
        data.append(vs[i].y());
        data.append(vs[i].z());
        // couleurs sommets
        data.append(colors.redF());
        data.append(colors.greenF());
        data.append(colors.blueF());
        // normales sommets
        data.append(normals[ind_nor[i-12]].x());
        data.append(normals[ind_nor[i-12]].y());
        data.append(normals[ind_nor[i-12]].z());
        //textures (absentes sur les facettes de la dent)
        data.append(0);
        data.append(0);
    }
}


void rouedentee::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs)
{
    m_vbo.bind();

    int stride = 11;
    program->setAttributeBuffer("posAttr", GL_FLOAT, 0 * sizeof(GLfloat), 3, stride * sizeof(GLfloat));
    program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 3, stride * sizeof(GLfloat));
    program->setAttributeBuffer("norAttr", GL_FLOAT, 6 * sizeof(GLfloat), 3, stride * sizeof(GLfloat));
    program->setAttributeBuffer("texAttr", GL_FLOAT, 9 * sizeof(GLfloat), 2, stride * sizeof(GLfloat));

    program->enableAttributeArray("posAttr");
    program->enableAttributeArray("colAttr");
    program->enableAttributeArray("norAttr");
    program->enableAttributeArray("texAttr");

    m_texture[1]->bind();
    program->setUniformValue("bTexture", true);
    for (int i = 0; i < this->nb_dents; ++i) {
        int offset = i * 28; // décalage à appliquer pour chaque dent
        glFuncs->glDrawArrays(GL_TRIANGLE_FAN, 0 + offset, 6);
    }
    m_texture[1]->release();

    m_texture[0]->bind();
    for (int i = 0; i < this->nb_dents; ++i) {
        int offset = i * 28; // décalage à appliquer pour chaque dent
        glFuncs->glDrawArrays(GL_TRIANGLE_FAN, 6 + offset, 6);
    }
    m_texture[0]->release();
    program->setUniformValue("bTexture", false);

    for (int i = 0; i < this->nb_dents; ++i) {
        int offset = i * 28; // décalage à appliquer pour chaque dent
        glFuncs->glDrawArrays(GL_QUADS, 12 + offset, 16);
    }

    program->disableAttributeArray("posAttr");
    program->disableAttributeArray("colAttr");
    program->disableAttributeArray("norAttr");
    program->disableAttributeArray("texAttr");
    m_vbo.release();
}

int rouedentee::getNb_dents() const
{
    return nb_dents;
}

float rouedentee::getInterRayon()
{
   return r1 + (r2-r1)/2;
}

float rouedentee::getInterCirclePerimeter()
{
    return getInterRayon() * 2 * M_PI;
}
