#include "maillon.h"
#include "box.h"
#include "cylindre.h"

#include <GL/gl.h>
#include <QDebug>
#include <QMatrix4x4>
#include <QVector>
#include <QVector3D>
#include <QColor>

maillon::maillon(float lengthBox)
{
    float rayonCylindre = 0.02;
    float epaisseurCylindre = 0.13;

    float widthBox = 0.1/2;
    float epBox = 0.1/10;
    this->lengthMaillon = lengthBox;

    QColor colorCylinder = QColorConstants::LightGray;
    QColor colorBox (128, 128, 255);

    c1 = new cylindre(rayonCylindre, epaisseurCylindre, colorCylinder);
    c2 = new cylindre(rayonCylindre, epaisseurCylindre, colorCylinder);
    b1 = new box(lengthBox, widthBox, epBox, colorBox);
    b2 = new box(lengthBox, widthBox, epBox, colorBox);
}

bool maillon::getIsInterne()
{
    return this->interne;
}

void maillon::setTransforms(QMatrix4x4 &cam_mat, QMatrix4x4 &shape_mat, QOpenGLShaderProgram *m_program)
{
    QMatrix4x4 mv_mat = cam_mat*shape_mat;
    m_program->setUniformValue("mvMatrix", mv_mat);

    QMatrix3x3 nor_mat = shape_mat.normalMatrix();
    m_program->setUniformValue("norMatrix", nor_mat);
}

void maillon::setInterne(bool isInterne)
{
    this->interne = isInterne;
}

float maillon::getLengthMaillon()
{
    return this->lengthMaillon;
}


void maillon::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs, QMatrix4x4 &cam_mat, QMatrix4x4 &shape_mat)
{
    QMatrix4x4 current_mat = shape_mat;
    shape_mat = current_mat;
    float translateWidthMaillon = this->getLengthMaillon()/2;
    float translateRadiusCylindre = 0.02;

    if (this->getIsInterne()) shape_mat.translate(0, 0, -0.07);
    else shape_mat.translate(0, 0, -0.05);
    setTransforms(cam_mat, shape_mat, program);
    b1->draw(program, glFuncs);
    setTransforms(cam_mat, shape_mat, program);

    shape_mat = current_mat;
    if (this->getIsInterne()) shape_mat.translate(0, 0, translateRadiusCylindre);
    else shape_mat.translate(0, 0, 0);
    setTransforms(cam_mat, shape_mat, program);
    b2->draw(program, glFuncs);
    setTransforms(cam_mat, shape_mat, program);

    if (this->getIsInterne()) {
        shape_mat = current_mat;
        shape_mat.translate(-translateWidthMaillon, 0, -translateRadiusCylindre);
        setTransforms(cam_mat, shape_mat, program);
        c1->draw(program, glFuncs);
        setTransforms(cam_mat, shape_mat, program);

        shape_mat = current_mat;
        shape_mat.translate(translateWidthMaillon, 0, -translateRadiusCylindre);
        setTransforms(cam_mat, shape_mat, program);
        c2->draw(program, glFuncs);
        setTransforms(cam_mat, shape_mat, program);
    }
}
