#include "princ.h"
#include "dialog.h"
#include <QDebug>

Dialog *dialog;
Princ::Princ(QWidget *parent) :
    QMainWindow(parent)

{
    setupUi(this);
    dialog = new Dialog(this);

    connect (glarea, SIGNAL(radiusChanged(double)), this, SLOT(setSliderRadius(double)));
    connect (glarea, SIGNAL(axisXChanged(double)), this, SLOT(setSpinAxisX(double)));
    connect (glarea, SIGNAL(axisYChanged(double)), this, SLOT(setSpinAxisY(double)));
    connect (glarea, SIGNAL(axisZChanged(double)), this, SLOT(setSpinAxisZ(double)));
    connect (glarea, SIGNAL(accelerationChanged(double)), this, SLOT(setSpinAcceleration(double)));
    connect (glarea, SIGNAL(distChanged(double)), this, SLOT(setSpinDist(double)));
    connect (sli_radius, SIGNAL(valueChanged(int)), this, SLOT(onSliderRadius(int)));

    connect(dialog->ui->value_axis_x, SIGNAL(valueChanged(double)), this, SLOT(on_spinAxisX_valueChanged(double)));
    connect(dialog->ui->value_axis_y, SIGNAL(valueChanged(double)), this, SLOT(on_spinAxisY_valueChanged(double)));
    connect(dialog->ui->value_axis_z, SIGNAL(valueChanged(double)), this, SLOT(on_spinAxisZ_valueChanged(double)));
    connect(dialog->ui->value_acceleration, SIGNAL(valueChanged(double)), this, SLOT(on_spinAcceleration_valueChanged(double)));
    connect(dialog->ui->value_dist, SIGNAL(valueChanged(double)), this, SLOT(on_spinDist_valueChanged(double)));

    setSliderRadius(glarea->getRadius());  // ceci va initialiser la position du Slider
}


void Princ::setSliderRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    int value = radius*20;
    if (sli_radius->value() != value) {
        qDebug() << "  sli_radius->setvalue()";
        sli_radius->setValue(value);
    }
}

void Princ::setSpinAxisX(double x)
{
    double value = x;
    if (dialog->ui->value_axis_x->value() != value) {
        qDebug() << "value_axis_x->setvalue()";
        dialog->ui->value_axis_x->setValue(value);
    }
}

void Princ::setSpinAxisY(double y)
{
    double value = y;
    if (dialog->ui->value_axis_y->value() != value) {
        qDebug() << "value_axis_y->setvalue()";
        dialog->ui->value_axis_y->setValue(value);
    }
}

void Princ::setSpinAxisZ(double z)
{
    double value = z;
    if (dialog->ui->value_axis_z->value() != value) {
        qDebug() << "value_axis_z->setvalue()";
        dialog->ui->value_axis_z->setValue(value);
    }
}

void Princ::setSpinAcceleration(double a)
{
    double value = a;
    if (dialog->ui->value_acceleration->value() != value) {
        qDebug() << "value_acceleration->setvalue()";
        dialog->ui->value_acceleration->setValue(value);
    }
}

void Princ::setSpinDist(double dist)
{
    qDebug() << __FUNCTION__ << dist << sender();
    double value = dist;
    if (dialog->ui->value_dist->value() != value) {
        qDebug() << "  spinDist->setvalue()";
        dialog->ui->value_dist->setValue(value);
    }
}

void Princ::onSliderRadius(int value)
{
    qDebug() << __FUNCTION__ << value << sender();
    qDebug() << "  emit radiusChanged()";
    emit glarea->radiusChanged(value/20.0);
}

void Princ::on_spinAxisX_valueChanged(double x)
{
    emit glarea->axisXChanged(x);
}

void Princ::on_spinAxisY_valueChanged(double y)
{
    emit glarea->axisYChanged(y);
}

void Princ::on_spinAxisZ_valueChanged(double z)
{
    emit glarea->axisZChanged(z);
}

void Princ::on_spinAcceleration_valueChanged(double a)
{
    emit glarea->accelerationChanged(a);
}

void Princ::on_spinDist_valueChanged(double dist)
{
    emit glarea->distChanged(dist);
}

void Princ::on_pushButton_clicked()
{
    dialog->ui->value_axis_x->setValue(glarea->getAxisX());
    dialog->ui->value_axis_y->setValue(glarea->getAxisY());
    dialog->ui->value_axis_z->setValue(glarea->getAxisZ());
    dialog->ui->value_acceleration->setValue(glarea->getAcceleration());
    dialog->ui->value_dist->setValue(glarea->getDist());
    dialog->show();
}
