#ifndef PATH_H
#define PATH_H

#include "qvectornd.h"
class path
{
public:
    path();
    virtual ~path() {};
    /**
     * @brief getDirection
     * @param ratio
     * @return direction of the path of 'maillons'
     */
    virtual QVector3D getDirection(double ratio);
    /**
     * @brief getPosition
     * @param ratio
     * @return position at a certain point of the path
     */
    virtual QVector3D getPosition(double ratio);
    /**
     * @brief getAngle
     * @param ratio
     * @return angle of the path
     */
    virtual double getAngle(double ratio);
    /**
     * @brief getLength
     * @return length of the path
     */
    virtual double getLength();
};

#endif // PATH_H
