#include "cylindre.h"

#include <GL/gl.h>
#include <QDebug>
#include <QMatrix4x4>
#include <QVector3D>
#include <QColor>
#include <math.h>

cylindre::cylindre(float rayon, float ep_cyl, QColor color)
{
    m_vbo.create();
    m_vbo.bind();
    QVector<GLfloat> vertData;
    buildVertData(vertData, rayon, ep_cyl, color);
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));
    m_vbo.release();
}

cylindre::~cylindre()
{
    m_vbo.destroy();
}

void cylindre::buildVertData(QVector<GLfloat> &data, float rayon, float ep_cyl, QColor color)
{
    float a = 360.0 / (float) nb_face;
    float rad = a * M_PI/180.0;

    /** Création des vertices **/
    QVector3D bottomCircleCenter (0, 0, -ep_cyl/2);
    QVector3D topCircleCenter (0, 0, ep_cyl/2);

    QVector<QVector3D> bottomCircleVertices;
    QVector<QVector3D> topCircleVertices;

    for (int i = 0; i < nb_face; ++i) {
        QVector3D verticeBottom = QVector3D(rayon * cos(rad * i), rayon * sin(rad * i), -ep_cyl/2);
        QVector3D verticeTop    = QVector3D(rayon * cos(rad * i), rayon * sin(rad * i), ep_cyl/2);
        bottomCircleVertices.append(verticeBottom);
        topCircleVertices.append(verticeTop);
    }

    /** Création des faces **/
    // face du bas
    QVector<QVector3D> vs;
    // face 1
    vs.append(bottomCircleCenter);
    vs.append(bottomCircleVertices);
    vs.append(bottomCircleVertices[0]);
    // face 2
    vs.append(topCircleCenter);
    vs.append(topCircleVertices);
    vs.append(topCircleVertices[0]);
    // faces
    for (int i = 0; i < nb_face + 1 ; ++i) {
        vs.append(bottomCircleVertices[i%nb_face]);
        vs.append(topCircleVertices[i%nb_face]);
    }

    /** Création des normales **/
    QVector<QVector3D> normales;
    // face 1
    normales.append(QVector3D(0, 0, -1));
    // face 2
    normales.append(QVector3D(0, 0, 1));
    // faces
    for (int i = 0; i < nb_face + 1; ++i) { // phong hack
        QVector3D center = (bottomCircleCenter);
        QVector3D vertex = (bottomCircleVertices[i%nb_face]);
        QVector3D normale = vertex - center;
        normale.normalize();
        normales.append(normale);
    }

    // formatte pour le buffer
    for (int i = 0; i < nb_face + 2; ++i) {
        // coordonnées sommets
        data.append(vs[i].x());
        data.append(vs[i].y());
        data.append(vs[i].z());
        // couleurs sommets
        data.append(color.redF());
        data.append(color.greenF());
        data.append(color.blueF());
        // normales sommets
        data.append(normales[0].x());
        data.append(normales[0].y());
        data.append(normales[0].z());
    }

    for (int i = nb_face+2; i < 2 * (nb_face+2); ++i) {
        // coordonnées sommets
        data.append(vs[i].x());
        data.append(vs[i].y());
        data.append(vs[i].z());
        // couleurs sommets
        data.append(color.redF());
        data.append(color.greenF());
        data.append(color.blueF());
        // normales sommets
        data.append(normales[1].x());
        data.append(normales[1].y());
        data.append(normales[1].z());
    }

    int indexNormaleDouble = 4;

    for (int i = 2 * (nb_face + 2); i < vs.size(); ++i) {
        // coordonnées sommets
        data.append(vs[i].x());
        data.append(vs[i].y());
        data.append(vs[i].z());
        // couleurs sommets
        data.append(color.redF());
        data.append(color.greenF());
        data.append(color.blueF());
        // normales sommets
        data.append(normales[indexNormaleDouble/2].x());
        data.append(normales[indexNormaleDouble/2].y());
        data.append(normales[indexNormaleDouble/2].z());

        indexNormaleDouble++;
    }
}


void cylindre::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs)
{
    m_vbo.bind();

    program->setAttributeBuffer("posAttr", GL_FLOAT, 0 * sizeof(GLfloat), 3, 9 * sizeof(GLfloat));
    program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 3, 9 * sizeof(GLfloat));
    program->setAttributeBuffer("norAttr", GL_FLOAT, 6 * sizeof(GLfloat), 3, 9 * sizeof(GLfloat));
    program->enableAttributeArray("posAttr");
    program->enableAttributeArray("colAttr");
    program->enableAttributeArray("norAttr");

    int circleVertexCount = nb_face + 2; // le vertex central + chaque vertex autour (nb_face) + le vertex en double pour fermer
    int faceVertexCount = nb_face*2 + 2; // duo de vertex par segment entre chaque face + les deux vertices fermantes
    glFuncs->glDrawArrays(GL_TRIANGLE_FAN, 0, circleVertexCount);
    glFuncs->glDrawArrays(GL_TRIANGLE_FAN, circleVertexCount, circleVertexCount);
    glFuncs->glDrawArrays(GL_QUAD_STRIP, circleVertexCount*2, faceVertexCount);

    program->disableAttributeArray("posAttr");
    program->disableAttributeArray("colAttr");
    program->disableAttributeArray("norAttr");

    m_vbo.release();
}
