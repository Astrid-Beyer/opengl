#ifndef ROUEDENTEE_H
#define ROUEDENTEE_H

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

class rouedentee
{
public:
    /**
     * @brief rouedentee : constructor
     * @param r1 : gear wheel's radius
     * @param r2 : gear wheel's teeth radius
     * @param ep_roue : gear wheel's thickness
     * @param nb_dents : gear wheel's number of teeth
     */
    rouedentee(float r1, float r2, float ep_roue, int nb_dents);
    ~rouedentee();
    /**
     * @brief buildVertData : build vertices, set colors, normals and texture
     * @param data
     * @param i
     * @param r1
     * @param r2
     * @param ep_roue
     * @param nb_dents
     */
    void buildVertData(QVector<GLfloat> &data, int i, float ep_roue, int nb_dents);
    /**
     * @brief draw : make the object gear wheel appears on the 3D scene
     * @param program
     * @param glFuncs
     */
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs);
    /**
     * @brief getNb_dents
     * @return number of teeth of the gear wheel
     */
    int getNb_dents() const;
    /**
     * @brief getInterRayon
     * @return radius at the center of each tooth
     */
    float getInterRayon();
    /**
     * @brief getInterCirclePerimeter
     * @return perimeter of the circle (going through each tooth's center)
     */
    float getInterCirclePerimeter();

private:
    QOpenGLBuffer m_vbo;
    int nb_dents;
    float r1;
    float r2;

    QOpenGLTexture *m_texture[2];
};

#endif // ROUEDENTEE_H
