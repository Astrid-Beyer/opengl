varying lowp  vec4 col;
varying highp vec3 nor;
varying mediump vec4 texc;
uniform bool bTexture;
uniform sampler2D texture;

void main() {
    // Couleur de la texture de base
    vec4 textureColor = vec4(1.0, 1.0, 1.0, 1.0);

    // LUMIERE DIRECTIONNELLE
    // Couleur de la lumière directionnelle
    vec3 lightColorDirectional = vec3 (1.0, 1.0, 1.0);
    // Direction de lumière directionnelle
    vec3 light = normalize(vec3(0.0, 0.0, 10.0));

    // LUMIERE AMBIANTE
    // Couleur de la lumière ambiante
    vec3 lightColorAmbiant = vec3 (0.1, 0.1, 0.2);

    // Normale du fragment, normalisée
    vec3 surfaceNormale = normalize(nor);

    // Cosinus de l'angle entre la normale et la lumière
    float cosTheta = clamp(dot(surfaceNormale, light), 0.0, 1.0);

    if(bTexture) textureColor = texture2D(texture, texc.st);

    vec4 lightSum = vec4(lightColorDirectional * cosTheta + lightColorAmbiant, 1.0);
    vec4 surfaceIntrinsicColor = textureColor * col;
    gl_FragColor = lightSum * surfaceIntrinsicColor;
}

