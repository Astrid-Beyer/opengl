#ifndef MAILLON_H
#define MAILLON_H

#include "box.h"
#include "cylindre.h"
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class maillon
{
public:
    /**
     * @brief maillon : constructor
     */
    maillon(float lengthBox);
    /**
     * @brief draw : make the object link (maillon) appears on the 3D scene
     * @param program
     * @param glFuncs
     * @param cam_mat
     * @param shape_mat
     * This function needs more parameters than other objects because it needs to use its own matrices in setTransforms()
     */
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs, QMatrix4x4 &cam_mat, QMatrix4x4 &shape_mat);
    /**
     * @brief setTransforms : apply translations and rotations made just before on the scene
     * @param cam_mat
     * @param shape_mat
     * @param m_program
     */
    void setTransforms(QMatrix4x4 &cam_mat, QMatrix4x4 &shape_mat, QOpenGLShaderProgram *m_program);
    /**
     * @brief getIsInterne
     * @return if the maillon is inner or not (true/false)
     */
    bool getIsInterne();
    /**
     * @brief setInterne
     * @param isInterne
     */
    void setInterne(bool isInterne);
    /**
     * @brief getLengthMaillon
     * @return length of the object
     */
    float getLengthMaillon();

private:
    QOpenGLBuffer m_vbo;
    bool interne;
    cylindre *c1 = nullptr;
    cylindre *c2 = nullptr;
    box *b1 = nullptr;
    box *b2 = nullptr;
    float lengthMaillon;
};

#endif // MAILLON_H
