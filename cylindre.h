#ifndef CYLINDRE_H
#define CYLINDRE_H

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class cylindre
{
public:
    /**
     * @brief cylindre : constructor
     * @param rayon cylinder's radius
     * @param ep_cyl cylinder's thickness
     * @param color
     */
    cylindre(float rayon, float ep_cyl, QColor color);
    ~cylindre();
    /**
     * @brief buildVertData : build vertices, set colors and normals
     * @param data
     * @param rayon
     * @param ep_cyl
     * @param color
     */
    void buildVertData(QVector<GLfloat> &data, float rayon, float ep_cyl, QColor color);
    /**
     * @brief draw : make the object cylinder appears on the 3D scene
     * @param program
     * @param glFuncs
     */
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs);
private:
    QOpenGLBuffer m_vbo;
    int nb_face = 8;
};

#endif // CYLINDRE_H
