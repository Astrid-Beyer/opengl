#include "segmentpath.h"

segmentPath::segmentPath(QVector3D start, QVector3D end)
{
    this->start = start;
    this->end = end;
}

QVector3D segmentPath::getDirection(double ratio)
{
    return (end - start).normalized();
}

QVector3D segmentPath::getPosition(double ratio)
{
    return (start * ratio + end * (1-ratio));
}


double segmentPath::getLength()
{
    return (end - start).length();
}
