#ifndef SEGMENTPATH_H
#define SEGMENTPATH_H

#include "path.h"

class segmentPath: public path
{
public:
    segmentPath(QVector3D start, QVector3D end);
    QVector3D getDirection(double ratio);
    QVector3D getPosition(double ratio);
    double getLength();

    QVector3D start;
    QVector3D end;
};

#endif // SEGMENTPATH_H
