#include "compoundedpath.h"
#include <QVector>

compoundedPath::compoundedPath(QVector<path*> paths)
{
    this->paths = new QVector<path*>(paths);
}

QVector3D compoundedPath::getDirection(double ratio)
{
    int pathCount = paths->length();
    double scaledRatio = ratio * pathCount;
    path* currentPath = (* paths)[(int) scaledRatio % pathCount];
    return currentPath->getDirection(std::fmod(ratio, 1.0));
}

QVector3D compoundedPath::getPosition(double ratio)
{
    int pathCount = paths->length();
    double scaledRatio = ratio * pathCount;
    path* currentPath = (* paths)[(int) scaledRatio % pathCount];
    return currentPath->getPosition(std::fmod(ratio, 1.0));
}

double compoundedPath::getAngle(double ratio)
{
    int pathCount = paths->length();
    double scaledRatio = ratio * pathCount;
    path* currentPath = (* paths)[(int) scaledRatio % pathCount];
    return currentPath->getAngle(std::fmod(ratio, 1.0));
}

double compoundedPath::getLength()
{
    double sum = 0;
    for (int i = 0; i < paths->length(); ++i) {
        sum += (*paths)[i]->getLength();
    }
}
