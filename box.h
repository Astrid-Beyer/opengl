#ifndef BOX_H
#define BOX_H

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class box
{
public:
    /**
     * @brief box : constructor
     * @param length
     * @param width
     * @param ep : thickness of the box
     * @param color
     */
    box(float length, float width, float ep, QColor color);
    ~box();
    /**
     * @brief buildVertData : build vertices, set colors and normals
     * @param data
     * @param length
     * @param width
     * @param ep
     * @param color
     */
    void buildVertData(QVector<GLfloat> &data, float length, float width, float ep, QColor color);
    /**
     * @brief draw : make the object box appears on the 3D scene
     * @param program
     * @param glFuncs
     */
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs);

private:
    QOpenGLBuffer m_vbo;
};

#endif // BOX_H
