#include "box.h"

#include <GL/gl.h>
#include <QDebug>
#include <QMatrix4x4>
#include <QVector>
#include <QVector3D>
#include <QColor>

box::box(float length, float width, float ep, QColor color)
{
    m_vbo.create();
    m_vbo.bind();
    QVector<GLfloat> vertData;
    buildVertData(vertData, length, width, ep, color);
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));
    m_vbo.release();
}

box::~box()
{
    m_vbo.destroy();
}

void box::buildVertData(QVector<GLfloat> &data, float length, float width, float ep, QColor color)
{
    float corner = 0.05/4;

    QVector<QVector3D> vs = { // vertices
                              QVector3D(length - corner, width, ep), // A 0
                              QVector3D(length, width - corner , ep), // B 1
                              QVector3D(length, -width + corner, ep), // C 2
                              QVector3D(length - corner, -width, ep), // D 3
                              QVector3D(-length+corner, -width, ep), // E 4
                              QVector3D(-length, -width + corner, ep), // F 5
                              QVector3D(-length, width - corner, ep), // G 6
                              QVector3D(-length + corner, width, ep), // H 7

                              QVector3D(length - corner, width, -ep), // A' 8
                              QVector3D(length, width - corner , -ep), // B' 9
                              QVector3D(length, -width + corner, -ep), // C' 10
                              QVector3D(length - corner, -width, -ep), // D' 11
                              QVector3D(-length+corner, -width, -ep), // E' 12
                              QVector3D(-length, -width + corner, -ep), // F' 13
                              QVector3D(-length, width - corner, -ep), // G' 14
                              QVector3D(-length + corner, width, -ep), // H' 15
                            };

    int ind_ver[] = { 7, 6, 5, 4, 3, 2, 1, 0,
                      8, 9, 10, 11, 12, 13, 14, 15,
                      2, 3, 11, 10,
                      3, 4, 12, 11,
                      4, 5, 13, 12,
                      15, 7, 0, 8,
                      0, 1, 9, 8,
                      1, 2, 10, 9,
                      5, 6, 14, 13,
                      6, 7, 15, 14,
                    };

    int ind_col[] = { 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0,
                    };

    QVector<QVector3D> normals;
    QVector<int> ind_nor;

    for (int i = 0; i < 16; i+=8)
    {
        QVector3D localA = vs[ind_ver[i]];
        QVector3D localB = vs[ind_ver[i+1]];
        QVector3D localC = vs[ind_ver[i+2]];

        QVector3D v1 = localB - localA;
        QVector3D v2 = localC - localA;
        QVector3D normalACB = QVector3D::normal(v1, v2);

        normals.append(normalACB);
    }

    for (int i = 16; i < 48; i+=4)
    {
        QVector3D localA = vs[ind_ver[i]];
        QVector3D localB = vs[ind_ver[i+1]];
        QVector3D localC = vs[ind_ver[i+2]];

        QVector3D v1 = localB - localA;
        QVector3D v2 = localC - localA;
        QVector3D normalACB = QVector3D::normal(v1, v2);

        normals.append(normalACB);
    }


    ind_nor = { 0, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1,
                2, 2, 2, 2,
                3, 3, 3, 3,
                4, 4, 4, 4,
                5, 5, 5, 5,
                6, 6, 6, 6,
                7, 7, 7, 7,
                8, 8, 8, 8,
                9, 9, 9, 9,
              };

    for (int i = 0; i < 48; ++i) {
        // coordonnées sommets
        data.append(vs[ind_ver[i]].x());
        data.append(vs[ind_ver[i]].y());
        data.append(vs[ind_ver[i]].z());
        // couleurs sommets
        data.append(color.redF());
        data.append(color.greenF());
        data.append(color.blueF());
        // normales sommets
        data.append(normals[ind_nor[i]].x());
        data.append(normals[ind_nor[i]].y());
        data.append(normals[ind_nor[i]].z());
    }
}

void box::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *glFuncs)
{
    m_vbo.bind();
    program->setAttributeBuffer("posAttr",
                                GL_FLOAT, 0 * sizeof(GLfloat), 3, 9 * sizeof(GLfloat));
    program->setAttributeBuffer("colAttr",
                                GL_FLOAT, 3 * sizeof(GLfloat), 3, 9 * sizeof(GLfloat));
    program->setAttributeBuffer("norAttr",
                                GL_FLOAT, 6 * sizeof(GLfloat), 3, 9 * sizeof(GLfloat));
    program->enableAttributeArray("posAttr");
    program->enableAttributeArray("colAttr");
    program->enableAttributeArray("norAttr");

    // Pour des questions de portabilité, hors de la classe GLArea, tous les appels
    // aux fonctions glBidule doivent être préfixés par glFuncs->.
    glFuncs->glDrawArrays(GL_POLYGON, 0, 8);
    glFuncs->glDrawArrays(GL_POLYGON, 8, 16);
    glFuncs->glDrawArrays(GL_QUADS, 16, 32);

    program->disableAttributeArray("posAttr");
    program->disableAttributeArray("colAttr");
    program->disableAttributeArray("norAttr");

    m_vbo.release();
}
