// CC-BY Edouard.Thiel@univ-amu.fr - 31/01/2022

#ifndef PRINC_H
#define PRINC_H

#include "ui_princ.h"

class Princ : public QMainWindow, private Ui::Princ
{
    Q_OBJECT

public:
    explicit Princ(QWidget *parent = 0);

private slots:
    void on_pushButton_clicked();
public slots:
    void setSliderRadius(double radius);
    void setSpinAxisX(double x);
    void setSpinAxisY(double y);
    void setSpinAxisZ(double z);
    void setSpinAcceleration(double a);
    void setSpinDist(double dist);

protected slots:
    void onSliderRadius(int value);
    void on_spinAxisX_valueChanged(double x);
    void on_spinAxisY_valueChanged(double y);
    void on_spinAxisZ_valueChanged(double z);
    void on_spinAcceleration_valueChanged(double a);
    void on_spinDist_valueChanged(double dist);
};

#endif // PRINC_H
