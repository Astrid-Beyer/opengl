#ifndef COMPOUNDEDPATH_H
#define COMPOUNDEDPATH_H

#include "path.h"

class compoundedPath : public path
{
public:
    compoundedPath(QVector<path*> paths);

    QVector3D getDirection(double ratio);
    QVector3D getPosition(double ratio);
    double getAngle(double ratio);

    double getLength();

    QVector<path*>* paths;

};

#endif // COMPOUNDEDPATH_H
