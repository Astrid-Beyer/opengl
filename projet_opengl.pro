#
# QtCreator project
#

QT += core gui widgets

equals     (QT_MAJOR_VERSION, 5): QT += opengl
greaterThan(QT_MAJOR_VERSION, 5): QT += openglwidgets

CONFIG += c++17

unix {
    LIBS += -lGLU
}
win32 {
    LIBS += -lGLU32 -lOpengl32
}

TARGET = runme
TEMPLATE = app

SOURCES += main.cpp\
        arcpath.cpp \
        box.cpp \
        compoundedpath.cpp \
        cylindre.cpp \
        dialog.cpp \
        kite.cpp \
        maillon.cpp \
        path.cpp \
        princ.cpp \
        glarea.cpp \
        rouedentee.cpp \
        segmentpath.cpp

HEADERS  += princ.h \
        arcpath.h \
        box.h \
        compoundedpath.h \
        cylindre.h \
        dialog.h \
        glarea.h \
        kite.h \
        maillon.h \
        path.h \
        rouedentee.h \
        segmentpath.h

FORMS    += princ.ui \
    dialog.ui

RESOURCES += \
    projet_opengl.qrc
