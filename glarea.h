// CC-BY Edouard.Thiel@univ-amu.fr - 31/01/2023

#ifndef GLAREA_H
#define GLAREA_H

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include "maillon.h"
#include "path.h"
#include "rouedentee.h"
#include "cylindre.h"
#include "box.h"

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    ~GLArea();
    double getRadius() { return m_radius; }

    double pathing(QMatrix4x4 world_mat, QMatrix4x4 cam_mat, QOpenGLFunctions *glFuncs, path *path, maillon *m, double offsetDistance);
public slots:
    void setRadius(double radius);
    void setAxisX(double x);
    void setAxisY(double y);
    void setAxisZ(double z);
    void setAcceleration(double a);
    void setDist(double dist);

    double getAxisX();
    double getAxisY();
    double getAxisZ();
    double getAcceleration();
    double getDist();

signals:
    void radiusChanged(double newRadius);
    void axisXChanged(double newX);
    void axisYChanged(double newY);
    void axisZChanged(double newZ);
    void accelerationChanged(double newA);
    void distChanged(double newDist);

protected slots:
    void onTimeout();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

private:
    double m_angle = 0;
    QTimer *m_timer = nullptr;
    double m_anim = 0;
    double m_radius = 0.5;
    double m_ratio = 1;
    double m_x = 0;
    double m_y = 0;
    double m_z = 0;
    double m_a = 0;
    double m_dist = 1.0;

    QOpenGLShaderProgram *m_program;

    void makeGLObjects();
    void tearGLObjects();
    void setTransforms(QMatrix4x4 &cam_mat, QMatrix4x4 &shape_mat);

    /** CREATES OBJECTS **/
    rouedentee *plateau = nullptr;
    rouedentee *pignon = nullptr;
    cylindre *baseManivelle = nullptr;
    cylindre *manivelle = nullptr;

    cylindre *basePedale = nullptr;
    cylindre *hauteurPedale = nullptr;
    cylindre *cotePedale = nullptr;
    box *pedale = nullptr;

    maillon *m = nullptr;
};

#endif // GLAREA_H
