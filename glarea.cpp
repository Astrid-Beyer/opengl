#include "glarea.h"
#include "arcpath.h"
#include "maillon.h"
#include "path.h"
#include "segmentpath.h"
#include <cmath>
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>
#include <QVector3D>

const char *vertexShaderFile   = ":/vertex.glsl";
const char *fragmentShaderFile = ":/fragment.glsl";

float rayonPlateau = 0.50;
float rayonPignon = 0.20;
float ep_roues = 0.13;
float tailleMaillon = 0.15/2;
float dist = 4.5;
bool maillon_interne = true;
QVector3D positionPlateau (1,0,0);
QVector3D positionPignon (-1,0,0);

GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);  // suréchantillonnage pour l'antialiasing
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    // slider
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));

    connect (this, SIGNAL(axisXChanged(double)), this, SLOT(setAxisX(double)));
    connect (this, SIGNAL(axisYChanged(double)), this, SLOT(setAxisY(double)));
    connect (this, SIGNAL(axisZChanged(double)), this, SLOT(setAxisZ(double)));
    connect (this, SIGNAL(distChanged(double)), this, SLOT(setDist(double)));
    connect (this, SIGNAL(accelerationChanged(double)), this, SLOT(setAcceleration(double)));

    // timer animation
    m_timer = new QTimer(this);
    m_timer->setInterval(50);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    //    m_timer->start();
}


GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete m_timer;
    makeCurrent();
    tearGLObjects();
    doneCurrent();
}


void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();

    glEnable(GL_DEPTH_TEST);
    makeGLObjects();

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }
    m_program->setUniformValue("texture", 0);
}


void GLArea::makeGLObjects()
{
    QColor colorManivelle (178, 178, 204);
    QColor colorPedale (190, 190, 204);
    plateau = new rouedentee(rayonPlateau, 0.55, ep_roues, 20);
    pignon = new rouedentee(rayonPignon, 0.25, ep_roues, 10);
    baseManivelle = new cylindre(0.02, 0.5, colorManivelle);

    basePedale = new cylindre(0.05, 0.1, colorManivelle);
    hauteurPedale = new cylindre(0.05, 0.7, colorManivelle);
    cotePedale = new cylindre(0.05, 0.15, colorManivelle);
    pedale = new box(0.2, 0.1, 0.04, colorPedale);

    float lengthBox = (plateau->getInterCirclePerimeter() / 360) * (360/plateau->getNb_dents());
    m = new maillon(lengthBox);
}


void GLArea::tearGLObjects()
{
    delete plateau;
    delete pignon;
    delete baseManivelle;
    delete manivelle;
    delete basePedale;
    delete hauteurPedale;
    delete cotePedale;
    delete pedale;
    delete m;
}


void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
}

double GLArea::pathing(QMatrix4x4 world_mat, QMatrix4x4 cam_mat, QOpenGLFunctions *glFuncs, path *path, maillon *m, double offsetDistance) {
    double steps = path->getLength() / m->getLengthMaillon();
    QVector3D unitZ (0,0,1);
    double totalDistance = offsetDistance;

    for (int i = 0; i <= steps ; ++i) {
        bool interne = (i%2) == 0;
        QMatrix4x4 shape_mat = world_mat;
        bool i_interne = interne;
        double ratio = fmod(totalDistance / path->getLength(), 1);
        QVector3D inter = path->getPosition(ratio);

        shape_mat.translate(inter);
        shape_mat.rotate(path->getAngle(ratio), unitZ);
        setTransforms(cam_mat, shape_mat);

        m->setInterne(i_interne);
        m->draw(m_program, glFuncs, cam_mat, shape_mat);
        totalDistance += m->getLengthMaillon();
    }
    return fmod(steps+offsetDistance, m->getLengthMaillon());
}

void GLArea::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    QOpenGLFunctions *glFuncs = context()->functions();  // cf initializeGL

    m_program->bind(); // active le shader program

    // Projection
    QMatrix4x4 proj_mat;
    GLfloat hr = m_radius, wr = hr * m_ratio;
    proj_mat.frustum(-wr, wr, -hr, hr, 1.0, 5.0);
    proj_mat.translate(0, 0, m_dist);
    m_program->setUniformValue("projMatrix", proj_mat);

    // Caméra
    QMatrix4x4 cam_mat;
    cam_mat.translate(0, 0, -3.0);

    // Orientation de la scène
    QMatrix4x4 world_mat;
    world_mat.rotate(m_x, 1, 0, 0);
    world_mat.rotate(m_y, 0, 1, 0);
    world_mat.rotate(m_z, 0, 0, 1);

    QMatrix4x4 shape_mat; // position d'un objet par rapport à la scène
    QMatrix4x4 shape_plateau; // position par rapport au plateau
    QMatrix4x4 shape_pignon; // position par rapport au pignon

    /** PLATEAU **/
    shape_mat = world_mat;
    shape_mat.translate(1, 0, 0);
    shape_mat.rotate(m_angle, 0, 0, 1);
    setTransforms(cam_mat, shape_mat);
    plateau->draw(m_program, glFuncs);

    shape_plateau = shape_mat;

    /** PEDALE **/
    shape_plateau.translate(0, 0, 0.1);
    setTransforms(cam_mat, shape_plateau);
    basePedale->draw(m_program, glFuncs);

    shape_plateau.rotate(90, 1, 0, 0);
    shape_plateau.translate(0, 0.1/2, 0);
    shape_plateau.translate(0, 0, 0.3);
    setTransforms(cam_mat, shape_plateau);
    hauteurPedale->draw(m_program, glFuncs);

    shape_plateau.rotate(90, 1, 0, 0);
    shape_plateau.translate(0, 0.7/2, -0.05/2);
    setTransforms(cam_mat, shape_plateau);
    cotePedale->draw(m_program, glFuncs);

    shape_plateau.rotate(90, 0, 1, 0);
    shape_plateau.translate(0.2, 0, 0);
    shape_plateau.rotate(-m_angle + 90, 1, 0, 0);
    setTransforms(cam_mat, shape_plateau);
    pedale->draw(m_program, glFuncs);

    /** PEDALE FOND **/
    shape_plateau = shape_mat;
    shape_plateau.translate(0, 0, -0.1);
    setTransforms(cam_mat, shape_plateau);
    basePedale->draw(m_program, glFuncs);

    shape_plateau.rotate(90, 1, 0, 0);
    shape_plateau.translate(0, -0.1/2, 0);
    shape_plateau.translate(0, 0, -0.3);
    setTransforms(cam_mat, shape_plateau);
    hauteurPedale->draw(m_program, glFuncs);

    shape_plateau.rotate(90, 1, 0, 0);
    shape_plateau.translate(0, -0.7/2, 0.05/2);
    setTransforms(cam_mat, shape_plateau);
    cotePedale->draw(m_program, glFuncs);

    shape_plateau.rotate(90, 0, 1, 0);
    shape_plateau.translate(-0.2, 0, 0);
    shape_plateau.rotate(-m_angle + 90, 1, 0, 0);
    setTransforms(cam_mat, shape_plateau);
    pedale->draw(m_program, glFuncs);

    /** PIGNON **/
    shape_mat = world_mat;
    shape_mat.translate(-1, 0, 0);
    shape_mat.rotate(m_angle * (plateau->getNb_dents() / pignon->getNb_dents()), 0, 0, 1);
    setTransforms(cam_mat, shape_mat);
    pignon->draw(m_program, glFuncs);

    /** MANIVELLE **/
    shape_mat = world_mat;
    shape_mat.translate(-1.1, 0, 0.16);
    shape_mat.rotate(145, 0, 1, 0);
    setTransforms(cam_mat, shape_mat);
    baseManivelle->draw(m_program, glFuncs);

    /** MAILLONS **/
    float epaisseurCentre = ep_roues/4;
    QVector3D departTopPlateau      = (positionPlateau + QVector3D(0, plateau->getInterRayon(), epaisseurCentre));
    QVector3D departTopPignon       = (positionPignon + QVector3D(0, pignon->getInterRayon(), epaisseurCentre));
    QVector3D departMidPignon       = (positionPignon + QVector3D(0, 0, epaisseurCentre));
    QVector3D departMidPlateau      = (positionPlateau + QVector3D(0, 0, epaisseurCentre));
    QVector3D departBottomPlateau   = (positionPlateau - QVector3D(0, plateau->getInterRayon(), -epaisseurCentre));
    QVector3D departBottomPignon    = (positionPignon - QVector3D(0, pignon->getInterRayon(), -epaisseurCentre));

    segmentPath pathPignonToPlateau(departTopPignon, departTopPlateau);
    segmentPath pathPlateauToPignon(departBottomPlateau, departBottomPignon);
    arcPath pathAroundPignon(departMidPignon, pignon->getInterRayon(), 90, 270);
    arcPath pathAroundPlateau(departMidPlateau, plateau->getInterRayon(), -90, 90);

    float offsetInit = m_angle * plateau->getInterCirclePerimeter()/360;
    offsetInit = pathing(world_mat, cam_mat, glFuncs, &pathPignonToPlateau, m, offsetInit);
    offsetInit = pathing(world_mat, cam_mat, glFuncs, &pathAroundPlateau, m, offsetInit);
    offsetInit = pathing(world_mat, cam_mat, glFuncs, &pathPlateauToPignon, m, offsetInit);
    offsetInit = pathing(world_mat, cam_mat, glFuncs, &pathAroundPignon, m, offsetInit);

    m_program->release();
}

void GLArea::setTransforms(QMatrix4x4 &cam_mat, QMatrix4x4 &shape_mat)
{
    QMatrix4x4 mv_mat = cam_mat*shape_mat;
    m_program->setUniformValue("mvMatrix", mv_mat);

    QMatrix3x3 nor_mat = shape_mat.normalMatrix();
    m_program->setUniformValue("norMatrix", nor_mat);
}


void GLArea::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key()) {
    case Qt::Key_I :
        if (ev->text() == "I") {
            m_x += 1;
            setAxisX(m_x+1);
            if (m_x >= 360)
                m_x -= 360;
        } else  {
            m_x -= 1;
            setAxisX(m_x-1);
            if (m_x <= 0)
                m_x += 360;
        }
        update();
        break;
    case Qt::Key_J :
        if (ev->text() == "J") {
            m_y += 1;
            setAxisY(m_y+1);
            if (m_y >= 360)
                m_y -= 360;
        } else  {
            m_y -= 1;
            setAxisY(m_y-1);
            if (m_y <= 0)
                m_y += 360;
        }
        update();
        break;
    case Qt::Key_K :
        if (ev->text() == "K") {
            m_z += 1;
            setAxisZ(m_z+1);
            if (m_z >= 360)
                m_z -= 360;
        } else  {
            m_z -= 1;
            setAxisZ(m_z-1);
            if (m_z <= 0)
                m_z += 360;
        }
        update();
        break;
    case Qt::Key_Z :
        if (ev->text() == "Z") {
            if (m_dist <= 10) {
                m_dist += 0.1;
                setDist(m_dist+0.1);
            }
        } else {
            if (m_dist >= 0) {
                m_dist -= 0.1;
                setDist(m_dist-0.1);
            }
        }
        update();
        break;
    case Qt::Key_D :
        if (!(m_timer->isActive())) {
            m_timer->start();
            setAcceleration(5.0);
        }
        update();
        break;
    case Qt::Key_A :
        if (m_timer->isActive()) {
            if (ev->text() == "A")
                setAcceleration(m_a + 5.0);
            else if (m_a > 5) setAcceleration(m_a - 5.0);
        }
        update();
        break;
    case Qt::Key_S :
        if (m_timer->isActive()) {
            m_timer->stop();
            setAcceleration(0.0);
        }
        update();
        break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    (void) ev;
}


void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->position().x() << ev->position().y() <<
                ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->position().x() << ev->position().y() <<
                ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->position().x() << ev->position().y();
}


void GLArea::onTimeout()
{
    m_angle -= getAcceleration();
    qDebug() << m_angle;
    if (m_angle <= 0) m_angle = 360;
    update();
}


void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        update();
    }
}

void GLArea::setAxisX(double x)
{
    if (x != m_x && x > 0.01 && x <= 360) {
        m_x = x;
        qDebug() << "  emit axisXChanged()";
        emit axisXChanged(x);
        update();
    }
}

void GLArea::setAxisY(double y)
{
    if (y != m_y && y > 0.01 && y <= 360) {
        m_y = y;
        qDebug() << "  emit axisYChanged()";
        emit axisYChanged(y);
        update();
    }
}

void GLArea::setAxisZ(double z)
{
    if (z != m_z && z > 0.01 && z <= 360) {
        m_z = z;
        qDebug() << "  emit axisZChanged()";
        emit axisZChanged(z);
        update();
    }
}

void GLArea::setAcceleration(double a)
{
    if (a != m_a && a >= 0 && a <= 30) {
        m_a = a;
        qDebug() << "  emit accelerationChanged()";
        emit accelerationChanged(a);
        update();
    }
}

void GLArea::setDist(double dist)
{
    qDebug() << __FUNCTION__ << dist << sender();
    if (dist != m_dist && dist > -5 && dist <= 20) {
        m_dist = dist;
        qDebug() << "  emit distChanged()";
        emit distChanged(dist);
        update();
    }
}

double GLArea::getAxisX()
{
    return m_x;
}

double GLArea::getAxisY()
{
    return m_y;
}

double GLArea::getAxisZ()
{
    return m_z;
}

double GLArea::getAcceleration()
{
    return m_a;
}

double GLArea::getDist()
{
    return m_dist;
}
