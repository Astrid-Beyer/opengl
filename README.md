# Projet OpenGL
## Animation d'un pédalier de vélo
Projet de programmation graphique.
Le but de ce projet est d’animer un pédalier de vélo en utilisant Qt avec OpenGL. Il est demandé expressément de ne **pas** utiliser de VAO (ni de stocker des VBO dans un VAO).

## Outils utilisés
- C++ (Qt)
- OpenGL
- VBO, shaders

## Contraintes
- Centrer le pédalier sur la vue
- Faire une classe par élément simple qui doivent pouvoir se dessiner, bien centrés sur l'origine
- Utiliser deux textures (une pour chaque facette des roues dentées)
- Faire une boîte de dialogue non modale (couplage faible Qt) affichant les valeurs et permettant ces commandes (disponibles au clavier aussi) :
  - Tourner la scène autour des 3 axes
  - Diminuer/augmenter la distance caméra
  - Accélerer/ralentir la cadence du pédalier
- Les touches "d" et "s" permettent respectivement de faire tourner et stopper le pédalier.
- La chaîne est composée de maillons internes et externes
- Scène éclairée avec une lumière diffuse et une lumière ambiante
- Lissage de Phong pour les facettes des cylindres

*[Voir les détails...](https://pageperso.lis-lab.fr/~edouard.thiel/ens/opengl/pg04-tp.pdf)*

## Fonctionnement
![Lancement de l'application](https://media.discordapp.net/attachments/698203739022032906/1079451293271204000/lancemet_app.png?width=1061&height=627 "SettingUpApp"){: .shadow}

Au lancement, on peut voir dores et déjà la scène 3D avec le pédalier. La vue est par défaut de face.
Un bouton "Options" invite l'utilisateur à modifier des paramètres pour s'orienter sur la vue et agir sur le pédalier.


![Utilisation de l'application](https://media.discordapp.net/attachments/698203739022032906/1079451293053096076/utilisation_app.png?width=1061&height=627 "UsingApp"){: .shadow}

Les raccourcis claviers sont précisés sur la fenêtre dialog. L'accélération est capée jusqu'à 30.0, 360 pour les axes et 10.0 pour la distance caméra.

![Gif](https://media.discordapp.net/attachments/698203739022032906/1079455420319539301/Peek_26-02-2023_18-29.gif "Gif"){: .shadow}
### Auteur
Astrid BEYER  
M1 Informatique, parcours GIG Aix-Marseille Université