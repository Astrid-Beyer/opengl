#include "arcpath.h"

arcPath::arcPath(QVector3D center, double rayon, double depart, double end)
{
    this->center = center;
    this->rayon = rayon;
    this->depart = depart;
    this->end = end;
}

QVector3D arcPath::getDirection(double ratio)
{
    QVector3D norm = (getPosition(ratio)- center).normalized();
    return QVector3D (-norm.y(), norm.x(), 0);
}

QVector3D arcPath::getPosition(double ratio)
{
    double totalAngle = end - depart;
    double angle = totalAngle * ratio + depart;
    double cosAngle = cos(angle * M_PI/180);
    double sinAngle = sin(angle * M_PI/180);
    QVector3D unitPos (cosAngle, sinAngle, 0);

    return unitPos * rayon + center;
}


double arcPath::getLength()
{
    double ratio = (end - depart)/360;
    return (rayon*2) * M_PI * ratio;
}

