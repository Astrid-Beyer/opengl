#include "path.h"
#include <QDebug>

path::path()
{

}

QVector3D path::getDirection(double ratio)
{

}

QVector3D path::getPosition(double ratio)
{

}

double path::getAngle(double ratio)
{
    QVector3D dir = getDirection(ratio);
    QVector3D axis (1,0,0);
    double cosinus = QVector3D::dotProduct(dir, axis);
    QVector3D cross = QVector3D::crossProduct(dir, axis);
    double unsignedAngle = acos(cosinus) * 180/M_PI;

    if (cross.z() > 0) return -unsignedAngle;
    else return unsignedAngle;
}

double path::getLength()
{

}
