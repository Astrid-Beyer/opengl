#ifndef ARCPATH_H
#define ARCPATH_H

#include "path.h"

class arcPath : public path
{
public:
    arcPath(QVector3D center, double rayon, double depart, double end);
    QVector3D getDirection(double ratio);
    QVector3D getPosition(double ratio);
    double getLength();

    QVector3D center;
    double rayon;
    double depart;
    double end;
};

#endif // ARCPATH_H
